FROM ubuntu:xenial

ARG DEBIAN_FRONTEND=noninteractive

# create non-privileged user for building packages
RUN adduser --home=/debian-builds --shell=/bin/sh --disabled-password \
    --disabled-login builder

RUN apt update && \
    apt install -y \
        ca-certificates \
        wget

# add UBports repository
RUN printf 'deb [signed-by=/usr/share/keyrings/ubports-keyring.gpg] http://repo2.ubports.com/ xenial main\n' > \
    /etc/apt/sources.list.d/ubports.list && \
    wget -O /usr/share/keyrings/ubports-keyring.gpg \
    'https://repo.ubports.com/keyring.gpg'

# copy latest mk-build-deps from upstream since the devscripts package pulls in
# many unwanted dependencies
RUN wget -O /usr/local/bin/mk-build-deps.pl \
    'https://salsa.debian.org/debian/devscripts/-/raw/master/scripts/mk-build-deps.pl?inline=false' && chmod 0755 /usr/local/bin/mk-build-deps.pl

# mk-build-deps requires libdpkg-perl and equivs
# build.sh requires iproute2
RUN apt update && \
    apt upgrade -y && \
    apt install -y --no-install-recommends \
        build-essential \
        equivs \
        fakeroot \
        iproute2 \
        libdpkg-perl

COPY --chmod=0755 *.sh /usr/local/bin/
